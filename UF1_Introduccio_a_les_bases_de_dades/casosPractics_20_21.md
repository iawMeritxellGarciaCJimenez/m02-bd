## Exercicis disseny de bases de dades.
### Habitatges
Dissenyar un esquema E/R que reculli l'organització d'un sistema d'informació en el qual es vol tenir la informació sobre municipis, habitatges i persones. Cada persona només pot habitar en un habitatge, però pot ser propietària de més d'una. També ens interessa la interrelació de les persones amb el seu cap de família.
Feu els supòsits semàntics (`requeriments d'usuari`) complementaris necessaris.
***
### Carreteres
Dissenyar una base de dades que contingui informació relativa a totes les carreteres d'un determinat país. Es demana realitzar el disseny en el model E/R, sabent que:
- En aquest paıs les carreteres es troben dividides en trams.
- Un tram sempre pertany a una única carretera i no pot canviar de carretera.
- Un tram pot passar per diversos termes municipals, sent una dada d'interès al km. del tram pel qual entra en dit terme municipal i al km. pel qual surt.
- Hi ha una sèrie d'àrees en les que s'agrupen els trams, cada un dels quals no pot pertànyer a més d'una àrea.  

Establiu els atributs que considereu oportuns i d'ells, trieu l'`indentificador` adient per a cada entitat.

### Zoos  
Es vol dissenyar una base de dades relacional per emmagatzemar informació relativa als zoos existents en el món, així com les espècies i animals que aquests alberguen.
- De cada zoo es vol emmagatzemar el seu codi, nom, la ciutat i país on es troba, mida (m2) i pressupost anual.
- Cada zoo codifica els seus animals amb un codi propi, de manera que entre zoos es pot donar el cas que es repeteixi.
- De cada espècie animal s'emmagatzema un codi d'espècie, el nom vulgar, el nom científic, família a la qual pertany i si es troba en perill d'extinció.
- A més, s'ha de guardar informació sobre cada animal que els zoos tenen, com el seu número d'identificació, espècie, sexe, any de naixement, país d'origen i continent.

Establiu els atributs que considereu oportuns i d'ells, trieu l'`indentificador` adient per a cada entitat.
***

### Gestió de projectes

Es demana dissenyar una base de dades que reculli la informació sobre la gestió de projectes que es porta a terme a l'empresa i la participació dels empleats. De cada empleat necessitarem saber el seu DNI, nom, adreça, telèfon i projecte en el qual participa. Cada projecte s'identificarà per un codi que serà únic, tindrà un títol, una durada estimada, una durada real, un pressupost.
Interessa saber els empleats que desenvolupen i els que dirigeixen un projecte.

De cada projecte es vol saber el director del projecte, de manera que la mateixa persona pot dirigir diferents projectes. Modificar l'esquema anterior per recollir els nous requeriments d'usuari.

Es desitja saber de cada empleat qui és el seu cap en cas que el tingui. Modificar l'esquema anterior per recollir els nous requeriments d'usuari.

Cada empleat està assignat a un únic departament. De cada departament es vol conèixer el nom i la descripció.

Es demana l'esquema E/R amb les seves entitats, atributs i interrelacions ben descrites.
Nota: determineu les claus primàries que considereu oportú.
---

### Càtedres d'Universitat
Dissenyar una base de dades que reculli l'organització d'una Universitat. Es considera que:
- Els departaments poden estar en una sola facultat o ser interfacultatius,
agrupant en aquest cas càtedres que pertanyen a facultats diferents.
- Una càtedra es troba en un únic departament.
- Una càtedra pertany a una sola facultat.
- Un professor està sempre assignat a un únic departament i adscrit a una o diverses càtedres, podent canviar de càtedra, però no de departament. Interessa la data en què un professor és adscrit a una càtedra.
- Hi ha àrees de coneixement, i tot departament tindrà una única àrea de coneixement.
***
### CLUB NÀUTIC

Es vol dissenyar una base de dades relacional per a gestionar les dades dels socis d'un club nàutic. De cada soci es guarden les dades personals i les dades del vaixell o vaixells que posseeix: número de matrícula, nom, nombre de l'amarratge i quota que paga pel mateix.
Cada sortida amb vaixell que es fa des del club nàutic s'anota: data (amb hora inclosa), les dades del patró (qui porta el vaixell) i la destinació (on va). El patró pot ser soci o no soci. Es pot donar el cas que un patró soci pot tripular un vaixell que no sigui seu.
****
### Sucursal bancària
Es desitja dissenyar una base de dades per a una sucursal bancària que contingui informació sobre els clients, els comptes, les sucursals i les transaccions produïdes. Construir el model E/R tenint en compte les següents restriccions:
- Una transacció ve determinada pel seu nombre de transacció, la data i la quantitat.
- Un client pot tenir diversos comptes.
- Cada comptes pot tindre més d'un client com a titular.
- Un compte només pot pertànyer a una sucursal.
***
### ETT
Desitgem informatitzar la informació que manipula una ETT sobre empreses que ofereixen llocs de treball i persones que busquen feina. Les empreses ofereixen llocs de treball, informant de la professió sol·licitada, el lloc de treball destinat i les condicions exigides per a aquest lloc. De les persones que busquen feina tenim el seu DNI, nom, estudis i professió desitjada. Ens interessa saber quines persones poden optar a un lloc de treball, és a dir, poden participar en el procés de selecció. La persona interessada en un lloc es podrà apuntar per fer una entrevista per a aquest lloc. Per a cada lloc de treball es poden inscriure totes les persones interessades.
De cada entrevista, volem saber com ha anat, és a dir, emmagetzemar anotacions sobre si el candidat és idoni o no per al lloc de treball. En alguns casos es formalitzaran contractes entre les empreses i les persones, i emmagatzemarem la data de signatura, durada i sou del contracte. Se suposa que una persona només pot ser contractada per una empresa. de
l'empreses tenim les dades de CIF, nom i sector. A més, es distingiran pimes i multinacionals: de les primeres emmagatzemarem la ciutat en la qual s'ubica i de les segones el nombre de països en els quals té representació.
***
### Docència
Es desitja dissenyar una base de dades per a una Universitat que contingui informació sobre els alumnes, les assignatures i els professors. Construir un model E/R tenint en compte les següents restriccions:
- Una assignatura pot ser impartida per molts professors (no alhora) ja que poden existir grups. Un professor pot donar classes de moltes assignatures.
- Un alumne pot estar matriculat en moltes assignatures.
- Es necessita tenir constància de les assignatures en què està matriculat un
alumne, la nota obtinguda i el professor que l'ha qualificat.
- També cal tenir constància de les assignatures que imparteixen tots els professors (independentment de si tenen algun alumne matriculat en el seu grup).
- No hi ha assignatures amb el mateix nom.
Un alumne no pot estar matriculat en la mateixa assignatura amb dos professors diferents.
***
### Metro de Barcelona
Construir el model E/R que reflecteixi tota la informació necessària per a la gestió de les línies de metro d'una determinada ciutat. Els supòsits semàntics considerats són els següents:
- Una línia està composta per una sèrie d'estacions. De la línia volem saber el codi (L1, L2, etc) i el nom (Linia vermella, línia morada) i així respetimanet.
- Cada estació pertany almenys a una línia, podent pertànyer a diverses. Un exemple sería  l'`estació La Sagrera`, on la L1 (línia vermella) i la L5 (línia blava) comparteien aquesta estació.
- Una estació en un moment donat només pertany a una línia, però en el passat ha pogut formar part d'altra línia. Per exemple, donat un moment de la història recenta de Barcelona, va haver-hi un canvi d'estacions entre la lila i la groga. Interessarà saber doncs dataInici i dataFi de quan una estació ha format part d'una línia en concret.
- Cada estació pot tenir diversos accessos, però considerem que un accés només pot pertànyer a una estació.
- Un accés mai podrà canviar d'estació.
- Cada línia té assignats una sèrie de trens, no podent succeir que un tren estigui assignat a més d'una línia, però sí que no estigui assignat a cap (p. Ex., Si es troba en reparació).
- Algunes estacions tenen assignades cotxeres, i cada tren té assignada una cotxera.
- Interessa conèixer tots els accessos de cada línia. De cada accés, a part del codi i nom, voldrem saber si hi té ascensor.

Ampliació:

- De cada estació ens interessa saber quina posició ocupa a dintre de la línia segons el sentit. Així doncs, per l'`estació Sagrera` tenim el següent:
  - per a la **L1** direcció _Fondo_ `Sagrera` ocupa el lloc 23 però si és sentit _Bellvitge_ ocupa el lloc 8.
  - per a la **L5** direcció _Cornellà Centre_  `Sagrera` ocupa el lloc 9 però si és direcció _Vall d'Hebron_ ocupa el lloc 18.

***

### Biblioteca

Una biblioteca que encara manté manualment la gestió dels prèstecs dels llibres vol informatitzar el procés. Actualment es tenen fitxes de dos tipus:

- Fitxes on es recullen les característiques dels llibres Títol, Autor@/@s, Editorial, Any de publicació,
- Fitxes relatives als préstecs que s'han efectuat, títol del llibre, la persona a la qual se li ha prestat, la data de préstec i la de devolució.

A més d'aquestes fitxes, hem recollit mitjaçant entrevistes amb els usuaris informació que ens caldrà analitzar:

Per als llibres interessa saber, a més del que apareix actualment a les fitxes, l'idioma en què estan escrits.

A més, es desitjarà fer cerques en funció de paraules clau, de manera que podrem trobar un llibre a través de diferents paraules clau. Per exemple, el llibre Sistemes i bases de dades seria un dels llibres que ens sortiria a la consulta per paraula clau "model semàntic".

De cada llibre poden haver-hi diferents exemplars, on cadascú tindrà un codi propi que serà únic i es voldrà també recollir característiques físiques propiciades per l'ús (té la tapa trencada, cal enquadernar, té trencada el gomet anti-robatori, etc.).

Cada llibre tractarà d'un o diversos temes, el que interessa reflectir per poder realitzar consultes del tipus "Llibres o articles que tenim sobre Bases de dades Multimèdia", "Articles que podem consultar sobre el llenguatge QUEL", etc. Els temes es poden dividir en subtemes i així successivament (no se sap en quants nivells). Per exemple, en el tema de DISSENY podem distingir una sèrie de subtemes, com DISSENY CONCEPTUAL, DISSENY LÒGIC, DISSENY FÍSIC, DISSENY GRÀFIC, DISSENY ASSISTIT PER ORDINADOR, etc. Cada tema tindrà un nom i una descripció.

Dels autors, a més del nom, ens interessa conèixer la seva nacionalitat. De les editorials emmagatzemarem el seu nom, adreça, ciutat i país.

A la biblioteca volem distingir tres tipus de socis: alumnes, als que com a màxim se'ls prestarà una obra durant tres dies, alumnes de doctorat o projectes de fi de carrera, que tindran accés com a màxim a dues obres durant la setmana i els professors i altres biblioteques, als quals se'ls deixarà tres obres durant un termini màxim d'un mes.
De cada un d'ells emmagatzemarem seu DNI, Nom, Adreça, Telèfon, Titulació en què estan estudiant (cas de ser estudiants) o impartint classes (cas de ser professors).

Els codis seran els propis del nostre sistema, excepte el DNI i l'ISBN. S'ha de contemplar la major quantitat d'informació possible (_el més proper a la realitat_).

Proposar un esquema entitat-relació adient, identificant clarament entitats, atributs i interrelacions.
***

### CINÈFIL

Un cinèfil aficionat a la informàtica vol crear una Base de Dades que reculli informació diversa sobre el món cinematogràfic, des dels orígens del cinema fins a avui mateix, amb el contingut que es descriu a continuació.
Lògicament, vol tenir classificades moltes pel.lícules, que vindran identificades per un codi. També vol de cadascuna el nom, l’any de l’estrena, el pressupost, el director, etc. A més, de cada pel.lícula vol conéixer també quins actors van intervenir, així com el paper que hi representàven (actor principal, secundari, etc.) i el possible premi que va rebre per la seva interpretació.
Les pel.lícules són d’un tema determinat. Es ben sabut que hi ha actors especialitzats en un tema, encara que un actor és capaç d’interpretar varis temes amb diferent “habilitat”.
Com que el nostre cinèfil és una mica curiós, vol emmagatzemar també dades personals dels actors, que ha anat recollint al llegir revistes del món artístic. Per exemple, quins actors són en certa manera substitutius d’altres, amb un grau de possible substitució que pot anar de 1 a 10. També quins actors són “incompatibles”, o sigui, que mai han treballat ni treballaran junts amb una mateixa pel.lícula o escena.
Els actors estan contractats, en un moment donat per una companyia, però poden canviar si tenen una oferta millor. També poden retornar a una companyia en la que ja hi  havien treballat. Les companyies produeixen pel.lícules, però cap pel.lícula és coproduïda per dues o més companyies.
Com que el nostre amic fa molt de turisme, vol saber, per a cada ciutat, quines companyies hi tenen representació i a quina adreça. Evidentment, les companyies solen tenir representació a quasi totes les ciutats importants. Al mateix temps, vol també informació de quines pel.lícules s’estan rodant a cada ciutat i en quin moment, tenint en compte que una pel.lícula es pot rodar a vàries ciutats i també a una mateixa ciutat en diferents fases del seu rodatge.

Proposar un esquema entitat-relació adient (identificant clarament entitats, atributs i interrelacions) i un esquema lògic amb el seu DR.


## Exercicis extra


### Club nàutic
Es vol dissenyar una base de dades relacional per a gestionar les dades dels socis d'un club nàutic. De cada soci es guarden les dades personals i les dades del vaixell o vaixells que posseeix: número de matrícula, nom, número de l'amarratge i quota que paga pel mateix. A més, es vol mantenir informació sobre les sortides realitzades per cada vaixell, com la data i hora de sortida, el destí i les dades personals del patró, que no ha de ser el propietari del vaixell, ni cal que sigui soci del club.

### Gestió de publicacions de congressos

Una empresa especialitzada en l'organització de congressos ens ha demanat que li dissenyem una part d'un sistema software per gestionar el procés de selecció dels articles que són enviats a un congrés.


Els articles s'identifiquen per títol i tenen un cert nombre de pàgines. Un article és escrit per una o diverses persones (identificades per DNI i de qui se'n sap el nom) i es pot enviar a un congrés (identificat per nom i de qui es guarda també el lloc on es farà) en una certa data. Els congressos es composen de sessions identificades pel nom i de les que se'n sap la sala.

Inicialment, un article enviat a un congrés està en avaluació. L'avaluació la fan tres persones que no són els autors i que han d'assignar una puntuació a l'article cadascuna. Un article en avaluació pot ser rebutjat o acceptat. En cas que es rebutgi, el sistema enregistra el motiu de no acceptació. Si s'accepta, es guarda la sessió del congrés a la que s'assigna.

A més, es vol saber quins articles han estat acceptats en quins congressos.


---
### Publicitat
Es desitja confeccionar una base de dades que emmagatzemi informació sobre la publicitat actualment emesa en els principals mitjans de comunicació. Hi ha tres tipus de suports publicitaris: televisió, ràdio i premsa escrita. Dels anuncis es coneix l'eslògan que utilitzen. Si l'anunci és televisiu o és una falca radiofònica, es desitja conèixer quants minuts dura, el nom de les cadenes de televisió o emissores de ràdio respectivament que l'emeten i quantes vegades al dia és emès en cada un dels mitjans. Si l'anunci és imprès s'emmagatzemarà el nom i el tiratge de les publicacions. Dels anuncis impresos podran tenir imatge o no. Un anunci pertany a una campanya publicitària que pot incloure altres anuncis. Cada campanya publicitària té un tema (venda d'un producte, promoció del turisme en una determinada zona, ajuda a determinats països, prevenció de malalties, ...) i un pressupost total per a tots els anuncis que inclou la mateixa. Aquesta campanya publicitària la contracta a un anunciant del que coneixem el seu nom i si és institució o empresa.

Finalment es vol contemplar quins dels mitjans audiovisuals (és a dir cadenes de televisió i emissores de ràdio) considerats abans són al seu torn empreses que s'anuncien.

Nota: establiu les claus primàries que considereu oportunes.
***
